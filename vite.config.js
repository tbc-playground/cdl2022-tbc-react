import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    reporters: ['default', 'junit'],
    coverage: {
      provider: 'istanbul',
    },
    outputFile: {
      junit: './reports/node-test.xunit.xml',
    },
  },
});
